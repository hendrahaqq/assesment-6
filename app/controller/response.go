package controller

import "github.com/gin-gonic/gin"

func responseAPI(str interface{}, count int) gin.H {
	return gin.H{
		"message": "success",
		"result":  str,
		"count":   count,
	}
}
