package main

import (
	"assesment-5-golang-restful-api/app/config"
	"assesment-5-golang-restful-api/app/controller"
	"assesment-5-golang-restful-api/app/models"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	// koneksi
	db := config.Connect()
	strDB := controller.StrDB{DB: db}
	// migration
	models.Migrations(db)

	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		// office
		v1.POST("/office", strDB.CreateOffice)
		v1.DELETE("/office/:id", strDB.DeleteOffice)
		v1.PUT("/office/:id", strDB.UpdateOffice)
		v1.GET("/office/", strDB.GetOffice)
		v1.GET("/office/:id/show", strDB.GetOneOffice)
		v1.GET("/office-search", strDB.GetSearchOffice)
		// assesment-6
		v1.GET("/office-users/:id", strDB.GetOfficeUsers) //get all user in an office

		// user
		v1.POST("/user", strDB.CreateUser)
		v1.DELETE("/user/:id", strDB.DeleteUser)
		v1.PUT("/user/:id", strDB.UpdateUser)
		v1.GET("/user/", controller.Auth, strDB.GetUser)
		v1.GET("/user/:id/show", strDB.GetOneUser)
		v1.GET("/user-search", strDB.GetSearchUser)
		v1.POST("/login", strDB.LoginUser)
		//assesment-6
		v1.GET("/user-office", strDB.UserOffice)        //get all user and office
		v1.GET("/user-office/:id", strDB.GetUserOffice) //get one user and office
		v1.GET("/user-todo/:id", strDB.UserTodoList)    //get todo list on user

		// todo
		v1.POST("/todo", strDB.CreateTodos)
		v1.DELETE("/todo/:id", strDB.DeleteTodos)
		v1.PUT("/todo/:id", strDB.UpdateTodos)
		v1.GET("/todo/", strDB.GetTodos)
		v1.GET("/todo/:id/show", strDB.GetOneTodos)
		v1.POST("/todo-search", strDB.LoginUser)
		//assesment-6
		v1.GET("/todo-user", strDB.TodosUser)        //get all todo and user
		v1.GET("/todo-user/:id", strDB.GetTodosUser) //get one todo and user
	}
	router.Run(":8080")
}
